<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>test tags</title>
</head>
<body>
  <form:form method="post" modelAttribute="contentModel">
     username:<form:input path="username"/><br>
     password:<form:password path="password"/><br>
     boolean checkbox: <form:checkbox path="testBoolean"/><br>
     array checkbox:
     <form:checkbox path="testArray" value="arrayItem 路人甲"/>
     arrayItem 路人甲
     <form:checkbox path="testArray" value="arrayItem 路人乙"/>
     arrayItem 路人乙
     <form:checkbox path="testArray" value="arrayItem 路人丙"/>
     arrayItem 路人丙
     <form:checkbox path="testArray" value="arrayItem 路人丁"/>
     arrayItem 路人丁
     <br>
     array select checkbox:
     <form:checkboxes path="selectArray" items="${contentModel.testArray }"/>
     <br>
     radiobutton:
     <form:radiobutton path="radiobuttonId" value="0"/>0
     <form:radiobutton path="radiobuttonId" value="1"/>1
     <form:radiobutton path="radiobuttonId" value="2"/>2
     <br>
     radiobuttons:
     <form:radiobuttons path="selectId" items="${contentModel.selectIds }"/>
     <br>
  </form:form>
</body>
</html>
package edu.neusoft.springmvc.controller;

import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/chartview")
public class ChartController {

  @RequestMapping("/test")
  public String getChart(ModelMap modelmap) {
    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    dataset.addValue(510, "深圳", "苹果");
    dataset.addValue(320, "深圳", "香蕉");
    dataset.addValue(580, "深圳", "橘子");
    dataset.addValue(390, "济南", "橙子");
    modelmap.addAttribute("type", 0);
    modelmap.addAttribute("dataset", dataset);
    return "test.chart";
  }
}

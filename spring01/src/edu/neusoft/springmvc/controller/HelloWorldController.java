package edu.neusoft.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {

  //http://localhost:8080/spring01/helloworld.action
  @RequestMapping("/helloworld")
  public String getHello() {
    return "success";
  }

  public String test() {
    return "test";
  }
}

package edu.neusoft.springmvc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.neusoft.springmvc.model.Address;
import edu.neusoft.springmvc.model.TestModel;
import edu.neusoft.springmvc.model.User;

@Controller
@RequestMapping("/model")
public class ModelController {

  @RequestMapping("/modelandview")
  public ModelAndView testModelAndView(TestModel model) {
    User user = new User();
    Address adr = new Address();
    //user.setUsername("zhangsan");
    adr.setCity("DL");

    Map<String, Object> map = new HashMap<String, Object>();
    map.put("model", model);
    map.put("user", user);
    map.put("address", adr);

    ModelAndView mv = new ModelAndView();
    mv.setViewName("showmodel");
    mv.addAllObjects(map);

    return mv;
  }

  @RequestMapping("/testmodelmap")
  public String testModelMap(TestModel model, ModelMap modelMap) {
    //modelMap.addAttribute("model", model);
    //modelMap.addAttribute(model);
    User user = new User();
    Address adr = new Address();
    //user.setUsername("zhangsan");
    adr.setCity("DL");

    Map<String, Object> map = new HashMap<String, Object>();
    map.put("model", model);
    map.put("user", user);
    map.put("address", adr);

    modelMap.addAllAttributes(map);
    return "showmodel";
  }
}

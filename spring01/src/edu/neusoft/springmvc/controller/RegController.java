package edu.neusoft.springmvc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.neusoft.springmvc.model.User;

@Controller
@RequestMapping("/register")
public class RegController {

  @RequestMapping("/getReg_bak")
  public String getReg(User user) {

    /* System.out.println("username:" + user.getUsername() + "\n" + "password:" + user.getPasword() + "\n" + "age:" + user.getAge() + "\n" + "province:"
      + user.getAddress().getProvince() + "\n" + "city:" + user.getAddress().getCity());*/
    return "success";
  }

  @RequestMapping("p/getReg_re")
  public void getReg(User user, HttpServletResponse response) {

    PrintWriter out = null;

    try {
      out = response.getWriter();
      /* out.print(user.getUsername() + "\n" + user.getPasword() + "\n" + user.getAddress().getProvince());*/
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      out.close();
    }
  }

  @RequestMapping("/getReg")
  public String getReg(User user, HttpServletRequest request) {

    request.setAttribute("user", user);
    return "regInfo";
  }

}

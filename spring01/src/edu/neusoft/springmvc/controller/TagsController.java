package edu.neusoft.springmvc.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.neusoft.springmvc.model.TagsModel;

@Controller
@RequestMapping("/tags")
public class TagsController {

  @RequestMapping("/test")
  public String test(Model model) {
    TagsModel tagsModel = new TagsModel();
    tagsModel.setUsername("zhangsan");
    tagsModel.setPassword("password");
    tagsModel.setTestBoolean(true);
    tagsModel.setTestArray(new String[] { "arrayItem 路人甲", "arrayItem 路人乙", "arrayItem 路人丙" });
    tagsModel.setSelectArray(new String[] { "arrayItem 路人甲" });
    tagsModel.setRadiobuttonId(1);
    tagsModel.setSelectId(2);
    tagsModel.setSelectIds(Arrays.asList(1, 2));
    Map<Integer, String> map = new HashMap<Integer, String>();
    map.put(1, "arrayItem 路人甲");
    map.put(2, "arrayItem 路人乙");
    map.put(3, "arrayItem 路人丙");
    tagsModel.setTestMap(map);
    tagsModel.setRemark("备注");

    model.addAttribute("contentModel", tagsModel);
    return "testtags";
  }
}

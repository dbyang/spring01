package edu.neusoft.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/test")
public class Test {

  @RequestMapping("/testRequestParam")
  public String testRequestParam(@RequestParam(value = "input1", required = true) int input1,
    @RequestParam(value = "input2", required = false) String input2) {
    System.out.println("test页面的输入值1是：" + input1);
    return "success";
  }

  @RequestMapping("/testRequestHeader")
  public String testRequestHeader(@RequestHeader("Accept-Encoding") String encoding) {
    System.out.println(encoding);
    return "success";
  }

  public String testCookie(@CookieValue("sessionid") String sessionid) {
    return "success";
  }

}

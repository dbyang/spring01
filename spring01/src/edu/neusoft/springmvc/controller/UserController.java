package edu.neusoft.springmvc.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.neusoft.springmvc.model.Course;
import edu.neusoft.springmvc.model.Student;
import edu.neusoft.springmvc.model.User;
import edu.neusoft.springmvc.service.StudentService;
import edu.neusoft.springmvc.service.UserService;

@Controller
@RequestMapping(path = "/user")
public class UserController {

  @Resource
  private UserService userService;

  @Resource
  private StudentService studentService;

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public String getUserById(@PathVariable("id") Integer id, ModelMap model) {
    User user = userService.getUserById(id);
    model.addAttribute("user", user);
    return "showUser";
  }

  @RequestMapping(value = "student", method = RequestMethod.GET)
  public String getStudentById(ModelMap model) {
    Student student = studentService.findStudentById("100000");
    List<Course> list = student.getCourseList();
    Course course = list.get(0);
    System.out.println(course.getCourseName());
    //model.addAttribute("user", user);
    return "success";
  }
}

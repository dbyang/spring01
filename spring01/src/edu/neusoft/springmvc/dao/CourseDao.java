package edu.neusoft.springmvc.dao;

import edu.neusoft.springmvc.model.Course;

public interface CourseDao {
    int deleteByPrimaryKey(Integer courseId);

    int insert(Course record);

    int insertSelective(Course record);

    Course selectByPrimaryKey(Integer courseId);

    int updateByPrimaryKeySelective(Course record);

    int updateByPrimaryKey(Course record);
}
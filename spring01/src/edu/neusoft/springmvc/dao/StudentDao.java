package edu.neusoft.springmvc.dao;

import edu.neusoft.springmvc.model.Student;

public interface StudentDao {
  int deleteByPrimaryKey(Integer stuId);

  int insert(Student record);

  int insertSelective(Student record);

  Student selectByPrimaryKey(Integer stuId);

  int updateByPrimaryKeySelective(Student record);

  int updateByPrimaryKey(Student record);

  Student findStudentById(String stuIdCard);
}
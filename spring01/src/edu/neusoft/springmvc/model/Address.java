package edu.neusoft.springmvc.model;

public class Address {

  public String province;

  public String city;

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public Address() {
    super();
    // TODO Auto-generated constructor stub
  }

  public Address(String province, String city) {
    super();
    this.province = province;
    this.city = city;
  }

}

package edu.neusoft.springmvc.model;

public class Course {
    private Integer courseId;

    private String courseName;

    private String courseDeleteFlg;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getCourseDeleteFlg() {
        return courseDeleteFlg;
    }

    public void setCourseDeleteFlg(String courseDeleteFlg) {
        this.courseDeleteFlg = courseDeleteFlg == null ? null : courseDeleteFlg.trim();
    }
}
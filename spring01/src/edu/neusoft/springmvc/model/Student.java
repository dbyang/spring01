package edu.neusoft.springmvc.model;

import java.util.List;

public class Student {
  private Integer stuId;

  private String stuIdCard;

  private String stuName;

  private Integer stuCourseId;

  private String stuDeleteFlg;

  private List<Course> courseList;

  public List<Course> getCourseList() {
    return courseList;
  }

  public void setCourseList(List<Course> courseList) {
    this.courseList = courseList;
  }

  public Integer getStuId() {
    return stuId;
  }

  public void setStuId(Integer stuId) {
    this.stuId = stuId;
  }

  public String getStuIdCard() {
    return stuIdCard;
  }

  public void setStuIdCard(String stuIdCard) {
    this.stuIdCard = stuIdCard == null ? null : stuIdCard.trim();
  }

  public String getStuName() {
    return stuName;
  }

  public void setStuName(String stuName) {
    this.stuName = stuName == null ? null : stuName.trim();
  }

  public Integer getStuCourseId() {
    return stuCourseId;
  }

  public void setStuCourseId(Integer stuCourseId) {
    this.stuCourseId = stuCourseId;
  }

  public String getStuDeleteFlg() {
    return stuDeleteFlg;
  }

  public void setStuDeleteFlg(String stuDeleteFlg) {
    this.stuDeleteFlg = stuDeleteFlg == null ? null : stuDeleteFlg.trim();
  }
}
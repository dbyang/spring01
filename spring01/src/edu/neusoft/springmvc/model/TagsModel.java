package edu.neusoft.springmvc.model;

import java.util.List;
import java.util.Map;

public class TagsModel {

  public String username;

  public String password;

  public boolean testBoolean;

  public String[] testArray;

  public String[] selectArray;

  public Integer radiobuttonId;

  public Integer selectId;

  public List<Integer> selectIds;

  public Map<Integer, String> testMap;

  public String remark;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isTestBoolean() {
    return testBoolean;
  }

  public void setTestBoolean(boolean testBoolean) {
    this.testBoolean = testBoolean;
  }

  public String[] getTestArray() {
    return testArray;
  }

  public void setTestArray(String[] testArray) {
    this.testArray = testArray;
  }

  public String[] getSelectArray() {
    return selectArray;
  }

  public void setSelectArray(String[] selectArray) {
    this.selectArray = selectArray;
  }

  public Integer getRadiobuttonId() {
    return radiobuttonId;
  }

  public void setRadiobuttonId(Integer radiobuttonId) {
    this.radiobuttonId = radiobuttonId;
  }

  public Integer getSelectId() {
    return selectId;
  }

  public void setSelectId(Integer selectId) {
    this.selectId = selectId;
  }

  public List<Integer> getSelectIds() {
    return selectIds;
  }

  public void setSelectIds(List<Integer> selectIds) {
    this.selectIds = selectIds;
  }

  public Map<Integer, String> getTestMap() {
    return testMap;
  }

  public void setTestMap(Map<Integer, String> testMap) {
    this.testMap = testMap;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public TagsModel(String username, String password, boolean testBoolean, String[] testArray, String[] selectArray, Integer radiobuttonId,
    Integer selectId, List<Integer> selectIds, Map<Integer, String> testMap, String remark) {
    super();
    this.username = username;
    this.password = password;
    this.testBoolean = testBoolean;
    this.testArray = testArray;
    this.selectArray = selectArray;
    this.radiobuttonId = radiobuttonId;
    this.selectId = selectId;
    this.selectIds = selectIds;
    this.testMap = testMap;
    this.remark = remark;
  }

  public TagsModel() {
    super();
    // TODO Auto-generated constructor stub
  }

}

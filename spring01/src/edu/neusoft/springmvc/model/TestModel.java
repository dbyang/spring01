package edu.neusoft.springmvc.model;

public class TestModel {

  public String str1;

  public String str2;

  public String str3;

  public String str4;

  public String getStr1() {
    return str1;
  }

  public void setStr1(String str1) {
    this.str1 = str1;
  }

  public String getStr2() {
    return str2;
  }

  public void setStr2(String str2) {
    this.str2 = str2;
  }

  public String getStr3() {
    return str3;
  }

  public void setStr3(String str3) {
    this.str3 = str3;
  }

  public String getStr4() {
    return str4;
  }

  public void setStr4(String str4) {
    this.str4 = str4;
  }

  public TestModel(String str1, String str2, String str3, String str4) {
    super();
    this.str1 = str1;
    this.str2 = str2;
    this.str3 = str3;
    this.str4 = str4;
  }

  public TestModel() {
    super();
    // TODO Auto-generated constructor stub
  }

}

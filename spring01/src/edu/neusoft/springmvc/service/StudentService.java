package edu.neusoft.springmvc.service;

import edu.neusoft.springmvc.model.Student;

public interface StudentService {

  public Student findStudentById(String stuIdCard);
}

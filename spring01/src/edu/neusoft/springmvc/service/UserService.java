package edu.neusoft.springmvc.service;

import edu.neusoft.springmvc.model.User;

public interface UserService {

  public User getUserById(int userid);
}

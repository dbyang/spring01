package edu.neusoft.springmvc.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import edu.neusoft.springmvc.dao.StudentDao;
import edu.neusoft.springmvc.model.Student;
import edu.neusoft.springmvc.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

  @Resource
  private StudentDao studentDao;

  @Override
  public Student findStudentById(String stuIdCard) {
    return this.studentDao.findStudentById(stuIdCard);
  }

}

package edu.neusoft.springmvc.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import edu.neusoft.springmvc.dao.UserDao;
import edu.neusoft.springmvc.model.User;
import edu.neusoft.springmvc.service.UserService;

@Service
public class UserServiceImpl implements UserService {

  @Resource
  private UserDao userDao;

  @Override
  public User getUserById(int userid) {
    return userDao.selectByPrimaryKey(userid);
  }

}

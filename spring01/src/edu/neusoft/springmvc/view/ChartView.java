package edu.neusoft.springmvc.view;

import java.awt.Font;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.springframework.web.servlet.view.AbstractView;

public class ChartView extends AbstractView {

  @Override
  protected void renderMergedOutputModel(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) throws Exception {
    JFreeChart chart = null;

    switch (((Integer) map.get("type")).intValue()) {
    case 0:
      chart = getChartBar(map);//构造柱状图
      break;
    case 1:
      break;
    case 2:
      break;
    default:
      break;
    }
    ChartUtilities.writeChartAsJPEG(response.getOutputStream(), chart, 400, 400);

  }

  public JFreeChart getChartBar(Map<String, Object> map) {
    JFreeChart chart = ChartFactory.createBarChart3D("产品销量图", "产品名称", "销量", (CategoryDataset) map.get("dataset"), PlotOrientation.VERTICAL, true,
      true, false);
    chart.getTitle().setFont(new Font("宋体", Font.BOLD, 20));
    chart.getCategoryPlot().getDomainAxis().setTickLabelFont(new Font("宋体", Font.BOLD, 11));
    chart.getCategoryPlot().getDomainAxis().setLabelFont(new Font("宋体", Font.BOLD, 11));
    chart.getCategoryPlot().getRangeAxis().setTickLabelFont(new Font("宋体", Font.BOLD, 11));
    chart.getCategoryPlot().getRangeAxis().setLabelFont(new Font("宋体", Font.BOLD, 11));
    chart.getLegend().setItemFont(new Font("宋体", Font.BOLD, 11));
    return chart;
  }

}

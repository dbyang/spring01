package edu.neusoft.springmvc.view;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.AbstractCachingViewResolver;

public class ChartViewResolver extends AbstractCachingViewResolver {
  private View chartview;

  private String chartSuffix;

  @Override
  protected View loadView(String viewName, Locale locale) throws Exception {
    View view = null;
    if (viewName.endsWith(this.getChartSuffix())) {
      view = this.getChartview();
    }
    return view;
  }

  public View getChartview() {
    return chartview;
  }

  public void setChartview(View chartview) {
    this.chartview = chartview;
  }

  public String getChartSuffix() {
    return chartSuffix;
  }

  public void setChartSuffix(String chartSuffix) {
    this.chartSuffix = chartSuffix;
  }

}
